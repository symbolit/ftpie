package main

import (
	"github.com/dutchcoders/goftp"
	"github.com/codegangsta/cli"
	"fmt"
	"os"
)

func listCommand(c *cli.Context) {
	ftp, err := goftp.Connect(c.GlobalString("server"))
	defer ftp.Close()

	if err != nil {
		panic(err)
	}

	err = ftp.Login(c.GlobalString("username"), c.GlobalString("password"))

	if err != nil {
		panic(err)
	}

	fmt.Println(ftp.Pwd())
}


func main() {

	app := cli.NewApp()
	app.Name = "ftpie"
	app.Usage = "Simple ftp command line client"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name: "username, u",
			Value: "anonymous",
			Usage: "username to conect with",
		},
		cli.StringFlag{
			Name: "password, p",
			Value: "",
			Usage: "password to connect with",
		},
		cli.StringFlag{
			Name: "server",
			Value: "localhost:21",
			Usage: "server to connect to (server-ip:port)",
		},
	}

	app.Commands = []cli.Command{
		cli.Command{
			Name: "list",
			Aliases: []string{"l"},
			Usage: "list current ftp directory",
			Action: listCommand,
		},
	}

	app.Run(os.Args)
}
